#pragma once
#include "NumerableSequence.h"
class Combination :
  public NumerableSequence
{
protected:
  std::vector<std::vector<ULL>> cnk;
  size_t k;
public:
  Combination();
  Combination(size_t n, size_t k);
  Combination(const std::vector<size_t> &combination, size_t n);
  Combination(ULL number, size_t n, size_t k);
  ~Combination();
  size_t GetK() const;
  std::vector<size_t> next() const;
  std::vector<size_t> prev() const;
};

