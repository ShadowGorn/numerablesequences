#pragma once
#include <vector>
#include <ostream>

using LL = long long;
using ULL = unsigned long long;

class NumerableSequence
{
protected:
  std::vector<size_t> a;
  ULL number;
  size_t n;
public:
  NumerableSequence(size_t n);
  NumerableSequence();
  ~NumerableSequence();
  virtual std::ostream& Print(std::ostream &out) const;
  virtual std::vector<size_t> GetSequence() const;
  virtual ULL GetNumber() const;
  virtual size_t GetN() const;
  virtual std::vector<size_t> next() const = 0;
  virtual std::vector<size_t> prev() const = 0;
};



