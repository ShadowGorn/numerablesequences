#include "Permutation.h"
#include <numeric>
#include <algorithm>


Permutation::Permutation() : Permutation(1)
{
}

Permutation::Permutation(size_t n) : NumerableSequence(n)
{
  fact = { 1 };
  for (size_t i = 1; i <= n; ++i) {
    fact.push_back(fact.back() * i);
  }
}

Permutation::Permutation(ULL number, size_t n) : Permutation(n)
{
  if (number > fact[n])
    return;

  this->number = number;
  size_t t = 1;
  std::vector<size_t> y(n);
  std::iota(y.begin(), y.end(), 1);

  while (t != n) {
    LL k = (number + fact[n - t] - 1) / fact[n - t] - 1;
    a[t - 1] = y[k];
    y.erase(y.begin() + k);
    number -= k*fact[n - t];
    t++;
  }
  a[n - 1] = y[0];
}

Permutation::Permutation(const std::vector<size_t>& permutation) : Permutation(permutation.size())
{
  this->a = permutation;
  number = 0;

  {
    std::vector<size_t> check = a;
    std::sort(check.begin(), check.end());
    std::vector<size_t> y(n);
    iota(y.begin(), y.end(), 1);
    if (check != y) {
      a = y;
      number = 1;
      return;
    }
  }

  for (size_t i = 0; i < n - 1; ++i) {
    size_t k = 0;
    for (size_t j = 0; j < i; ++j) {
      k += (a[j] > a[i]);
    }
    size_t a_ = (a[i] + k);

    if (i == n - 2) {
      number += a_ - (n - 2);
    }
    else {
      number += (a_ - (i + 1))*fact[(n - (i + 1))];
    }
  }
}


Permutation::~Permutation()
{
}

std::vector<size_t> Permutation::next() const
{
  //TODO: realize this
  return std::vector<size_t>();
}

std::vector<size_t> Permutation::prev() const
{
  //TODO: realize this
  return std::vector<size_t>();
}
