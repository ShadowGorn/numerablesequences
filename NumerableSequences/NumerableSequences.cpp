// NumerableSequences.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include "Combination.h"
#include "Permutation.h"

using namespace std;

int main()
{
  puts("1 - generate permutation by number");
  puts("2 - generate permutation number");
  puts("3 - generate combination by number");
  puts("4 - generate combination number");
  size_t mode;
  cin >> mode;

  if (mode == 1) {
    LL number, n;
    puts("Input size of permutation (n) and permutation number (I)");
    cin >> n >> number;
    Permutation p(number, n);
    puts("Permutation:");
    p.Print(cout);
  }
  else if (mode == 2) {
    int n;
    puts("Input size of permutation (n) and permutation (a)");
    cin >> n;
    vector<size_t> permutation(n);
    for (auto &it : permutation)
      cin >> it;
    Permutation p(permutation);
    puts("Permutation number:");
    cout << p.GetNumber() << endl;
  }
  else if (mode == 3) {
    LL number, n, k;
    puts("Input max possible element in combination (n), size of combination (k) and combination number (I)");
    cin >> n >> k >> number;
    Combination c(number, n, k);
    puts("Combination:");
    c.Print(cout);
  }
  else if (mode == 4) {
    size_t n, k;
    puts("Input max possible element in combination (n), size of combination (k) and permutation (a)");
    cin >> n >> k;
    vector<size_t> combination(k);
    for (auto &it : combination)
      cin >> it;
    Combination c(combination, n);
    puts("Combination number:");
    cout << c.GetNumber() << endl;
  }
  else {
    puts("Invalid argument");
  }
  return 0;
}

