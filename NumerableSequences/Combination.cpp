#include "Combination.h"
#include <algorithm>


Combination::Combination() : Combination(1, 1)
{
}

Combination::Combination(size_t n, size_t k) : NumerableSequence(k)
{
  this->n = n;
  this->k = k;

  cnk.assign(n + 1, std::vector<ULL>(n + 1, 1));
  for (size_t i = 1; i <= n; ++i) {
    for (size_t j = 1; j <= i - 1; ++j) {
      cnk[i][j] = cnk[i - 1][j - 1] + cnk[i - 1][j];
    }
  }
}

Combination::Combination(const std::vector<size_t>& combination, size_t n) : Combination(n, combination.size())
{
  {
    std::vector<size_t> check = combination;
    std::sort(check.begin(), check.end());
    check.resize(std::unique(check.begin(), check.end()) - check.begin());
    if (combination.size() != check.size()) {
      return;
    }
    for (auto it : check) {
      if (it > n) {
        return;
      }
    }
  }

  this->a = combination;
  for (size_t i = 0; i < k; ++i) {
    size_t start = (i > 0 ? combination[i - 1] : 0) + 1;
    for (size_t j = start; j < combination[i]; ++j) {
      number += cnk[n - j][k - i - 1];
    }
  }
}

Combination::Combination(ULL number, size_t n, size_t k) : Combination(n, k)
{
  if (number > cnk[n][k])
    return;

  this->number = number;
  ULL s = 0;
  size_t b = 0, t = 1;
  while (t <= k) {
    b++;
    while (b < n - k + t && s + cnk[n - b][k - t] < number) {
      s += cnk[n - b][k - t];
      b++;
    }
    a[t - 1] = b;
    t++;
  }

}

Combination::~Combination()
{
}

size_t Combination::GetK() const
{
  return k;
}

std::vector<size_t> Combination::next() const
{
  //TODO: realize this
  return std::vector<size_t>();
}

std::vector<size_t> Combination::prev() const
{
  //TODO: realize this
  return std::vector<size_t>();
}
