#include "NumerableSequence.h"
#include <numeric>


NumerableSequence::NumerableSequence(size_t n) : n(n), number(1)
{
  a.resize(n);
  std::iota(a.begin(), a.end(), 1);
}

NumerableSequence::NumerableSequence() : NumerableSequence(1)
{
}


NumerableSequence::~NumerableSequence()
{
}

std::ostream & NumerableSequence::Print(std::ostream & out) const
{
  for (auto it : a)
    out << it << " ";
  out << std::endl;
  return out;
}

std::vector<size_t> NumerableSequence::GetSequence() const
{
  return a;
}

ULL NumerableSequence::GetNumber() const
{
  return number;
}

size_t NumerableSequence::GetN() const
{
  return n;
}
