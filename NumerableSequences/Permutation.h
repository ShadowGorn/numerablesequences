#pragma once
#include "NumerableSequence.h"
class Permutation :
  public NumerableSequence
{
protected:
  std::vector<ULL> fact;
public:
  Permutation();
  Permutation(size_t n);
  Permutation(ULL number, size_t n);
  Permutation(const std::vector<size_t> &permutation);
  ~Permutation();
  std::vector<size_t> next() const;
  std::vector<size_t> prev() const;
};

